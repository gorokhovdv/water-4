<?php
/**
 * Created by PhpStorm.
 * User: Gorokohv D.V.
 * Date: 13.09.17
 * Time: 16:49
 */
namespace Water4;

Class Message
{
    static $entitySchemas = [
        "\Water4\MessageScheme\ManualFirstAfterReset",
        "\Water4\MessageScheme\MessageManualTest",
        "\Water4\MessageScheme\MessageManualStart",
        "\Water4\MessageScheme\MessageAutomatic12Our"
    ];
    static function loadMessageScheme(\Water4\BinaryString $stringObject)
    {
        foreach(self::$entitySchemas as $scheme){
            $callTest = [$scheme,"testScheme"];
            if(is_callable($callTest) && $callTest($stringObject) === true){
                $callResult = [$scheme,"getMessageResult"];
                if(is_callable($callResult) && ($result = $callResult($stringObject))){
                    return $result;
                }else{
                    if($result === false){
                        throw new \Exception("Scheme is Valid, but cant get result an any reason!");
                    }else{
                        throw new \Exception("Scheme ".$scheme." dont't have real result method!");
                    }
                }
            }
        }
        throw new \Exception("Can't found message scheme!");
    }
    static function Decode($message){
        try{
            $result = self::loadMessageScheme(new \Water4\BinaryString($message));
        } catch (\Exception $e) {
            $result = [
                "error" => $e->getMessage()
            ];
        }
        if(is_array($result)){
            return $result["error"];
        }
        return $result->renderResult();
    }
}
