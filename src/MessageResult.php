<?php
/**
 * Created by PhpStorm.
 * User: maestro
 * Date: 14.09.17
 * Time: 18:24
 */

namespace Water4;


class MessageResult
{
    private $iterator;
    public function setIterator($iterator){
        $this->iterator = $iterator;
    }
    public function renderResult(){
        return [
            "iterator" => $this->iterator
        ];
    }
}