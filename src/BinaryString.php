<?php
/**
 * Created by PhpStorm.
 * User: maestro
 * Date: 13.09.17
 * Time: 16:55
 */

namespace Water4;


class BinaryString
{
    protected $originalString;
    protected $stringLength;
    protected $mask;
    protected $currentByte;
    protected $currentBytePosition;
    protected $currentBitPosition;

    public function __construct($message)
    {
        $this->originalString = hex2bin($message);
        return $this->reset();
    }
    public function reset()
    {
        //$this->stringLength = strlen($this->originalString);
        $this->stringLength = count(str_split($this->originalString));
        $this->setMask();
        $this->currentBytePosition = 0;
        $this->currentBitPosition = 7;
        $this->currentByte = ($this->stringLength ? ord($this->originalString[$this->currentBytePosition]) : null);
        return $this;
    }
    public function readBit(){
        if ($this->currentByte === null) return null;
        $bit = ($this->currentByte & $this->mask[$this->currentBitPosition]) >> $this->currentBitPosition;
        if (--$this->currentBitPosition == -1)
        {
            $this->currentBitPosition = 7;
            $this->currentBytePosition++;
            $this->currentByte = $this->currentBytePosition < $this->stringLength ? ord($this->originalString[$this->currentBytePosition]) : null;
        }
        return $bit;
    }
    public function readBits($n)
    {
        $val = 0;
        while ($n--)
        {
            $bit = $this->readBit();
            if ($bit === null) return null;
            $val = ($val << 1) | $bit;
        }
        return $val;
    }
    public function readBitsToArray($n){
        $arr = [];
        while ($n--)
        {
            $bit = $this->readBit();
            if ($bit === null) return null;
            $arr[] = IntVal($bit);
        }
        return $arr;
    }
    public function readBitsToEnd()
    {
        $val = 0;
        while (true)
        {
            $bit = $this->readBit();
            if ($bit === null) return $val;
            $val = ($val << 1) | $bit;
        }
        return $val;
    }
    public function readBitsToEndByStepArray($n){
        $arr = [];
        while (true)
        {
            $bits = $this->readBits($n,true);
            if ($bits === null) return $arr;
            $arr[] = $bits;
        }
        return $arr;
    }
    public function subbit($count,$from = 0){
        $saveObject = $this->saveObject();
        $this->readBits($from);
        $val = $this->readBits($count);
        $this->restoreObject($saveObject);
        return $val;
    }
    public  function getSize(){
        return $this->stringLength;
    }
    protected function restoreObject($arr)
    {
        foreach($arr as $k => $val){
            $this->$k = $val;
        }
    }
    protected function saveObject()
    {
        return [
            "originalString" => $this->originalString,
            "stringLength" => $this->stringLength,
            "mask" => $this->mask,
            "currentByte" => $this->currentByte,
            "currentBytePosition" => $this->currentBytePosition,
            "currentBitPosition" => $this->currentBitPosition,
        ];
    }
    protected function setMask()
    {
        for($i = 1; $i<=8; $i++){
            if(count($this->mask) == 0){
                $this->mask[] = 1;
            }else{
                $this->mask[] = $this->mask[count($this->mask) - 1] * 2;
            }
        }
    }
}