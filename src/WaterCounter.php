<?php
/**
 * Created by PhpStorm.
 * User: denisgorokhov
 * Date: 14.09.17
 * Time: 21:53
 */

namespace Water4;


class WaterCounter
{
    static private $counterArray = [
        "firstChannel" => [
            "Count" => 272707,
            "startIterator" => 19
        ],
        "secondChannel" => [
            "Count" => 401912,
            "startIterator" => 21
        ],
        "12HourFirstChannel" => [
            1  => 0,
            2  => 0,
            3  => 0,
            4  => 0,
            5  => 0,
            6  => 0,
            7  => 0,
            8  => 0,
            9  => 0,
            10  => 0,
            11  => 0,
            12  => 0
        ],
        "12HourSecondChannel" => [
            1  => 0,
            2  => 0,
            3  => 0,
            4  => 0,
            5  => 0,
            6  => 0,
            7  => 0,
            8  => 0,
            9  => 0,
            10  => 0,
            11  => 0,
            12  => 0
        ],
    ];
    static function applyWaterCounter($arr){
        self::$counterArray["firstChannel"]["Count"] +=  ($arr["firstChannel12Hour"]-self::$counterArray["firstChannel"]["startIterator"]);
        self::$counterArray["secondChannel"]["Count"] +=  ($arr["secondChannel12Hour"]-self::$counterArray["secondChannel"]["startIterator"]);
        foreach(self::$counterArray["12HourFirstChannel"] as $k => $va ){
            if(isset($arr["hourMap"]["firstChannel"][$k])){
                self::$counterArray["12HourFirstChannel"][$k] = $arr["hourMap"]["firstChannel"][$k];
            }else{
                unset(self::$counterArray["12HourFirstChannel"][$k]);
            }
        }
        foreach(self::$counterArray["12HourSecondChannel"] as $k => $va ){
            if(isset($arr["hourMap"]["secondChannel"][$k])){
                self::$counterArray["12HourSecondChannel"][$k] = $arr["hourMap"]["secondChannel"][$k];
            }else{
                unset(self::$counterArray["12HourSecondChannel"][$k]);
            }
        }
        self::$counterArray["Iterator"] = $arr["Iterator"];
        self::$counterArray["firstChannel"]["Count"] = self::$counterArray["firstChannel"]["Count"]/1000;
        self::$counterArray["secondChannel"]["Count"] = self::$counterArray["secondChannel"]["Count"]/1000;
        return self::$counterArray;
    }
}