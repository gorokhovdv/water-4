<?php
/**
 * Created by PhpStorm.
 * User: maestro
 * Date: 14.09.17
 * Time: 15:26
 */

namespace Water4\MessageScheme;


class MessageAutomatic12Our extends \Water4\MessageScheme
{
    private $messageResult;

    static function testScheme(\Water4\BinaryString $stringObject){
        if($stringObject->getSize() > 6){
            return true;
        }
    }
    static function getMessageResult(\Water4\BinaryString $stringObject){
        return new \Water4\MessageScheme\MessageAutomatic12OurResult([
            "iterator" => $stringObject->readBits(4),
            "isStart" => $stringObject->readBits(1),
            "firstChannel12Hour" => $stringObject->readBits(17),
            "secondChannel12Hour" => $stringObject->readBits(17),
            "firstChannelHourMask" => $stringObject->readBitsToArray(12),
            "secondChannelHourMask" => $stringObject->readBitsToArray(12),
            "maxHourTotal" => $stringObject->readBits(3),
            "hourMap" => $stringObject
        ]);
    }
}