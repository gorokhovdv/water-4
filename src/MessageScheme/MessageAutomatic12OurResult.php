<?php
/**
 * Created by PhpStorm.
 * User: maestro
 * Date: 14.09.17
 * Time: 18:26
 */

namespace Water4\MessageScheme;


class MessageAutomatic12OurResult extends \Water4\MessageResult
{
    protected $isStart;
    protected $firstChannel12Hour;
    protected $secondChannel12Hour;
    protected $firstChannelHourMask;
    protected $secondChannelHourMask;
    protected $maxHourTotal;
    protected $hourMap;

    public function __construct($arr)
    {
        foreach($arr as $k => $val){
            switch($k){
                default : {
                    $callF = [$this,"set".ucfirst($k)];
                    if(is_callable($callF)){
                        $callF($val);
                    }
                    break;
                }
            }
        }
    }
    public function setIterator($val){
        $this->iterator = intval($val);
    }
    public function setIsStart($val){
        $this->isStart = intval($val);
    }
    public function setFirstChannel12Hour($val){
        $this->firstChannel12Hour = intval($val);
    }
    public function setSecondChannel12Hour($val){
        $this->secondChannel12Hour = intval($val);
    }
    public function setFirstChannelHourMask($val){
        $this->firstChannelHourMask = $val;
    }
    public function setSecondChannelHourMask($val){
        $this->secondChannelHourMask = $val;
    }
    public function setMaxHourTotal($val){
        $this->maxHourTotal = intval($val);
    }
    public function setHourMap($val){
        $hours = $val->readBitsToEndByStepArray($this->maxHourTotal);
        $arr = [
            "firstChannel" => [],
            "secondChannel" => [],
        ];
        $l = 0;
        foreach($this->firstChannelHourMask as $k => $mask){
            if($mask){
                $arr["firstChannel"][$k+1] = IntVal($hours[$l]);
                $l++;
            }
        }
        foreach($this->secondChannelHourMask as $k => $mask){
            if($mask){
                $arr["secondChannel"][$k+1] = IntVal($hours[$l]);
                $l++;
            }
        }
        $this->hourMap = $arr;
    }
    public function renderResult(){
        return [
            "Iterator" => $this->iterator,
            "isStart" => $this->isStart,
            "firstChannel12Hour" => $this->firstChannel12Hour,
            "secondChannel12Hour" => $this->secondChannel12Hour,
            "firstChannelHourMask" => $this->firstChannelHourMask,
            "secondChannelHourMask" => $this->secondChannelHourMask,
            "maxHourTotal" => $this->maxHourTotal,
            "hourMap" => $this->hourMap
        ];
    }
}